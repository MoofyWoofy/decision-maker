package com.example.decisions;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main2Activity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Spinner SpinnerCategory = findViewById(R.id.spinnerChoices);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.CategoryArray,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpinnerCategory.setAdapter(adapter);
        SpinnerCategory.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String data = parent.getItemAtPosition(position).toString();
        Toast.makeText(parent.getContext(), data, Toast.LENGTH_SHORT).show();


        Spinner SpinnerOptions = findViewById(R.id.spinnerChoices);
        TextView tv = findViewById(R.id.textDisplay);
        // Displaying Data inside the file
        try
        {
            String LoadSaveString = SpinnerOptions.getSelectedItem().toString();
            String filename = LoadSaveString + ".dat";

            File fa = new File(getFilesDir(),filename); //getFilesDir();
//            String fileAbsolutePath = fa.getAbsolutePath();
//            fileAbsolutePath = fileAbsolutePath + "\\" + filename;

            //  SHOWING THE DATA INSIDE THE FILE
            FileReader fr = new FileReader(fa);  //fileAbsolutePath
            int c;
            StringBuilder temp= new StringBuilder();
            while ((c=fr.read()) != -1)
            {
                temp.append((char) c);
            }
            tv.setText(temp.toString());
            fr.close();
        }
        catch (Exception e)
        {
            tv.setText("");
            Toast.makeText(parent.getContext(),data +" has no data inside it ",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    public void btnViewRandom(View view)
    {
        ArrayList<String> list = new ArrayList<String>();
        BufferedReader reader = null;
        try
        {
            Spinner SpinnerOptions = findViewById(R.id.spinnerChoices);
            String LoadSaveString = SpinnerOptions.getSelectedItem().toString();

            // Finding the file location to get the data
            String filename = LoadSaveString + ".dat";
            File fa = new File(getFilesDir(),filename); //getFilesDir();

            String line = "";
            reader = new BufferedReader(new FileReader(fa)); //Before was LoadSaveString
            while ((line = reader.readLine()) != null )
            {
                list.add(line);
            }

            //  Finding the Random Line
            Random rand = new Random();
            String RandomData = list.get(rand.nextInt(list.size()));
            TextView RandomDisplay = findViewById(R.id.textViewRandom);
            RandomDisplay.setText(RandomData);

            reader.close();
        }
        catch (Exception e)
        {
            Toast.makeText(getBaseContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }

    public void btnDeleteFile(View view)
    {
            Spinner spp = findViewById(R.id.spinnerChoices);
            String LoadSaveString = spp.getSelectedItem().toString();
            String filename = LoadSaveString + ".dat";

            File filetodelete = new File(getFilesDir(),filename);
            //deleteFile(fileAbsolutePath);
        if (filetodelete.exists())
        {
            filetodelete.delete();
            Toast.makeText(getBaseContext(), LoadSaveString + " has been cleared of data",Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(getBaseContext(), LoadSaveString + " has no data inside it",Toast.LENGTH_SHORT).show();
        }
    }

    public void btnDeleteLine(View view)
    {
        Spinner spp = findViewById(R.id.spinnerChoices);
        String LoadSaveString = spp.getSelectedItem().toString();
        String filename = LoadSaveString + ".dat";

        EditText edd = findViewById(R.id.editTextLineDelete);
        String lineToDelete = edd.getText().toString();
        String currentline;

        File deletelinefile = new File(getFilesDir(),filename);
        File tempfile  = new File(getFilesDir(), "tempfile.dat");

        try {
            BufferedReader reader = new BufferedReader(new FileReader(deletelinefile));
            BufferedWriter writer = new BufferedWriter(new FileWriter(tempfile));

            while ((currentline = reader.readLine()) != null)
            {
                // trim newline when comparing with lineToRemove
                String trimline = currentline.trim();
                if (trimline.equals(lineToDelete)) continue;
                writer.write(currentline + System.lineSeparator());
            }
            writer.close();
            reader.close();
            deletelinefile.delete();
            tempfile.renameTo(deletelinefile);
            Toast.makeText(getBaseContext(),lineToDelete + " has been deleted",Toast.LENGTH_SHORT).show();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Toast.makeText(getBaseContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }
}
