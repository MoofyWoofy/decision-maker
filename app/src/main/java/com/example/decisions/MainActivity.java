package com.example.decisions;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    // What is abstract? delete if necessary

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner SpinnerCategory = findViewById(R.id.spinnerCategory);
        ArrayAdapter<CharSequence> AdapterCategory = ArrayAdapter.createFromResource(this,R.array.CategoryArray, android.R.layout.simple_spinner_item);
        AdapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpinnerCategory.setAdapter(AdapterCategory);
        SpinnerCategory.setOnItemSelectedListener(this);
    }
    public void BtnGoToAnotherActivity(View v)
    {
        Intent i = new Intent(this,Main2Activity.class);
        startActivity(i);
    }
    public void btnAddInformation(View v) {
        EditText edd = findViewById(R.id.editTextData);
        Spinner spp = findViewById(R.id.spinnerCategory);
        Button addbtn = findViewById(R.id.btnAddinfo);

        String text1 = edd.getText().toString();
        String text2 = spp.getSelectedItem().toString();


        String filename = text2 + ".dat";

        try {
            if (text1.trim().length() == 0) {
//                addbtn.setEnabled(false);
                throw new CustomEx();
            }
            File fa = new File(getFilesDir(),filename); //getFilesDir();
            FileWriter fw = new FileWriter(fa,true); //fileAbsolutePath
            fw.write(text1 + System.lineSeparator());//  aka  "\n"
            fw.close();
            Toast.makeText(getBaseContext(), "Information Saved", Toast.LENGTH_SHORT).show();
        }
        catch (CustomEx ce){
            Toast.makeText(getApplicationContext(),"Enter Text!!",Toast.LENGTH_SHORT).show();
        }
        catch (Exception e)
        {
            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }   //End try catch statement
        File cachefiles = getCacheDir();
        cachefiles.delete();
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String data = parent.getItemAtPosition(position).toString();
        Toast.makeText(parent.getContext(), data, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

    class CustomEx extends Exception { }
